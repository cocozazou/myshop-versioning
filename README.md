<GRAS> <CHEVALIER>

# Etapes réalisées dans le TP
    - Construction de la classe Item
    - Construction de la classe Storage
    - Construction de la classe Shop
    - Construction de la classe Launch
    - Test des fonctionalités dans Launcher
    - Améliorations des 3 premières classes
    
    - Développements sur feature A et B
    - Merge et gestion des conflits
    - Test des fonctionalités dans Launcher
    
    - Construction class Consumable, Paper, Pen, Book, BookToTouch, MusicalBook, 
        PuzzleBook, OriginalBook
    - Construction nvlles fonctionnalités class Shop
    
    - Tests unitaires Junit
    - Packaging avec Jenkins sur branch dev et master
    
# Etapes non réalisées dans le TP
    
    
# Répartition des taches
 - Emile CHEVALIER
        - construction des classes
        - construction des tests Junit
        - accompagnement Corentin
        - packaging Jenkins

 - Corentin GRAS
        - Création du repo Gitlab
        - Initialisation du projet maven sur Intellij
        - Compréhension des classes et méthodes
        - Construction de tests Junit
        
# Classes couvertes par des tests unitaires :
    - Class Item
    - Class Storage
    - Class Shop
    

package com.ci.myshop.controller;

import com.ci.myshop.model.Item;

import java.util.HashMap;

public class Storage {

    private HashMap<String, Item> itemMap ;


    public Storage() {
        this.itemMap = new HashMap<String, Item>();
    }

    public void addItem(Item obj) {
        this.itemMap.put(obj.getName(), obj);
    }

    public Item getItem(String name) {
        if (this.itemMap.containsKey(name))
            return this.itemMap.get(name);
        else
            return new Item(name);
    }

    public HashMap<String, Item> getItemMap() {
        return this.itemMap;
    }

}

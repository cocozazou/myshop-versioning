package com.ci.myshop.controller;

import com.ci.myshop.model.Book;
import com.ci.myshop.model.Consumable;
import com.ci.myshop.model.Item;


import java.util.ArrayList;
import java.util.HashMap;

public class Shop {
    private Storage storage;
    private float cash;

    public float getCash() {
        return cash;
    }

    public Shop(Storage storage, float cash) {
        this.storage = storage;
        this.cash = cash;
    }

    public Item sell(String name) {
        if (this.storage.getItem(name).getName().equals("")) {
            System.out.println("erreur");
            return null;
        }

        this.storage.getItem(name).setNbrElt((this.storage.getItem(name).getNbrElt()) - 1);
        this.cash += this.storage.getItem(name).getPrice();
        return this.storage.getItem(name);

    }


    public boolean buy(Item objet) {
        float prix = objet.getPrice();
        if (prix > this.cash)
            return false;
        else {
            this.cash = this.cash - prix;
            return true;
        }
    }

    public Boolean isItemAvailable(String name) {
        if (this.storage.getItem(name).getNbrElt() > 0)
            return true;
        else
            return false;
    }

    public int getAgeForBook(String name) {
        if (this.storage.getItem(name) instanceof Book) {
            return ((Book) this.storage.getItem(name)).getAge();
        }
        else
            return -1;
    }

    public ArrayList<Book> getAllBook() {
        ArrayList<Book> result = new ArrayList<Book>();
        for (String  newName : this.storage.getItemMap().keySet()){
            if (this.storage.getItemMap().get(newName) instanceof Book)
                result.add((Book)this.storage.getItemMap().get(newName));
        }
        return result;
    }

    public int getNbItemInStorage(String name) {

        return this.storage.getItem(name).getNbrElt();
    }

    public int getQuantityPerConsumable(Consumable name) {
        if (name instanceof Consumable) {
            return name.getQuantity();
        }
        else
            return -1;
    }
}

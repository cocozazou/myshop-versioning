package com.ci.myshop;

import com.ci.myshop.controller.Shop;
import com.ci.myshop.controller.Storage;
import com.ci.myshop.model.Item;

import java.util.ArrayList;

public class Launch {
    public static void main(String[] args) {

        ArrayList<Item> items = new ArrayList<Item>();
        Item voiture = new Item("BMW", 120, 0);
        Item tv = new Item("LCD", 120, 5);
        Item radio = new Item("radio", 45, 12);

        items.add(tv);
        items.add(radio);
        items.add(voiture);

        Storage wharehouse = new Storage();
        for (Item pr : items) {
            wharehouse.addItem(pr);
        }

        Shop magasin = new Shop(wharehouse, 200);

        Item vendu = magasin.sell(tv.getName());

        System.out.println("Vente du produit : " + vendu.display());
        System.out.println("Cash restant : " + magasin.getCash());

        Item velo = new Item("vélo", 150, 15);
        Item moto = new Item("moto", 250, 15);
        System.out.println("Rachat d'un objet : " + magasin.buy(velo));
        System.out.println("Cash : " + magasin.getCash());
        System.out.println("Item avaible " + magasin.isItemAvailable("LCD"));

        System.out.println("Item avaible " + magasin.isItemAvailable("BMW"));


    }
}

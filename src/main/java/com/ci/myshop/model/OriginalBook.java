package com.ci.myshop.model;

public class OriginalBook extends Book{
    private boolean isNumeric;

    public OriginalBook(String name, float price, int nbrElt, int nbPage, String author, String publisher, int year, int age, boolean isNumeric) {
        super(name, price, nbrElt, nbPage, author, publisher, year, age);
        this.isNumeric = isNumeric;
    }

    public boolean isNumeric() {
        return isNumeric;
    }

    public void setNumeric(boolean numeric) {
        isNumeric = numeric;
    }
}

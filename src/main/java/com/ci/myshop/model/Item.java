package com.ci.myshop.model;

public class Item {

    public static int CountId = 0;

    private String name;
    private int id;
    private float price;
    private int nbrElt;

    public Item() {
        this.name = "";
    }

    public Item(String Name) {
        this.name = Name;
    }


    public Item(String name, float price, int nbrElt) {
        this.name = name;
        this.price = price;
        this.nbrElt = nbrElt;
        this.id = CountId++;
    }

    public String display() {
        String affichage = "";

        affichage = String.format(this.getName() + " " + this.getId() + " " +
                        this.getPrice() + " " + this.getNbrElt());
        return affichage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getNbrElt() {
        return this.nbrElt;
    }

    public void setNbrElt(int nbrElt) {
        this.nbrElt = nbrElt;
    }
}

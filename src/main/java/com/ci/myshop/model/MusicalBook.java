package com.ci.myshop.model;

import java.util.ArrayList;

public class MusicalBook extends Book{
    private ArrayList<String> listOfSound = new ArrayList<String>();
    private int lifetime;
    private int nbrBattery;

    public MusicalBook(String name, float price, int nbrElt, int nbPage, String author, String publisher, int year, int age, ArrayList<String> listOfSound, int lifetime, int nbrBattery) {
        super(name, price, nbrElt, nbPage, author, publisher, year, age);
        this.listOfSound = listOfSound;
        this.lifetime = lifetime;
        this.nbrBattery = nbrBattery;
    }

    public ArrayList<String> getListOfSound() {
        return listOfSound;
    }

    public void setListOfSound(ArrayList<String> listOfSound) {
        this.listOfSound = listOfSound;
    }

    public int getLifetime() {
        return lifetime;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    public int getNbrBattery() {
        return nbrBattery;
    }

    public void setNbrBattery(int nbrBattery) {
        this.nbrBattery = nbrBattery;
    }
}

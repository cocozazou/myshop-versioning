package com.ci.myshop.model;

public class Paper extends Consumable {
    private Quality quality;
    private float weight;

    public Paper(int quantity, Quality quality, float weight) {
        super(quantity);
        this.quality = quality;
        this.weight = weight;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}

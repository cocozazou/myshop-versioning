package com.ci.myshop.model;

public class PuzzleBook extends Book{
    private int nbrPiece;

    public PuzzleBook(String name, float price, int nbrElt, int nbPage, String author, String publisher, int year, int age, int nbrPiece) {
        super(name, price, nbrElt, nbPage, author, publisher, year, age);
        this.nbrPiece = nbrPiece;
    }

    public int getNbrPiece() {
        return nbrPiece;
    }

    public void setNbrPiece(int nbrPiece) {
        this.nbrPiece = nbrPiece;
    }
}

package com.ci.myshop.model;

public class Consumable extends Item{
    private int quantity;

    public Consumable() {
        this.quantity = 0;
    }

    public Consumable(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

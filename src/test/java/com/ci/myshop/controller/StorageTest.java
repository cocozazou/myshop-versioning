package com.ci.myshop.controller;

import com.ci.myshop.model.Item;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class StorageTest {

    private static Item tv = new Item("Télé", 120, 5);
    private static Storage str = new Storage();
    private static Item vide = new Item();

    @Test
    void addItem() {
        str.addItem(tv);
        assertEquals(tv, str.getItem("Télé"));
    }

    @Test
    void getItem() {
        str.addItem(tv);
        assertEquals(tv , str.getItem("Télé"));
    }
    @Test
    void getItemMap(){
        HashMap<String, Item>  test = new HashMap<String, Item>();
        test.put("Télé", tv);
        str.addItem(tv);
        assertEquals(str.getItemMap(), test);
    }
}
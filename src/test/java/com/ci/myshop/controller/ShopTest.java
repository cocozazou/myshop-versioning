package com.ci.myshop.controller;

import com.ci.myshop.model.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ShopTest {


    private static Storage str = new Storage();
    private static Shop magasin = new Shop(str, 500);
    Item voiture = new Item("BMW", 200000,1);


    @Test
    void sell() {
    Shop myMag = new Shop(str, 500);
    Item tv = new Item("Télé", 120, 5);
    str.addItem(tv);
    Item test;
    test = myMag.sell("Télé");
    assertEquals(tv, test);
    assertEquals(620, myMag.getCash());
    assertEquals(4, tv.getNbrElt());
    }

    @Test
    void getCash() {
        assertEquals(500, magasin.getCash());
    }

    @Test
    void buy() {
        Item tv = new Item("Télé", 120, 5);
        assertEquals(false, magasin.buy(voiture));

        assertEquals(true, magasin.buy(tv));

        assertEquals(380, magasin.getCash());
    }

    @Test
    void isItemAvailable() {
        str.addItem(voiture);
        assertEquals(true, magasin.isItemAvailable("BMW"));
        voiture.setNbrElt(0);
        assertEquals(false,magasin.isItemAvailable("BMW") );

    }

    @Test
    void getAgeForBook() {
        Book livre = new Book("chaperon", 12, 3,152,"Emile Zola","la redoute",1921,12);
        str.addItem(livre);

        assertEquals(-1, magasin.getAgeForBook("télé"));
        assertEquals(12, magasin.getAgeForBook("chaperon"));

    }

    @Test
    void getAllBook() {
        Book livre = new Book("chaperon", 12, 3, 152, "Emile Zola", "la redoute", 1921, 12);
        Book bd = new Book("vendredi soir", 15, 4, 42, "zazou", "la riviere", 2010, 18);

        str.addItem(bd);
        str.addItem(livre);
        str.addItem(voiture);
        ArrayList<Book> test = new ArrayList<Book>();
        test.add(livre);
        test.add(bd);
        assertEquals(test,magasin.getAllBook());
    }

    @Test
    void getNbItemInStorage() {
        Item tv = new Item("Télé", 120, 5);
        str.addItem(tv);
        assertEquals(5, magasin.getNbItemInStorage("Télé"));
    }


    @Test
    void getQuantityPerConsumable() {
    Paper papier = new Paper(100, Quality.HIGH, 80);
    Pen stylo = new Pen(80, "rouge", 20);
    Item tv = new Item("Télé", 120, 5);
    str.addItem(papier);
    str.addItem(stylo);
    str.addItem(tv);

    assertEquals(80, magasin.getQuantityPerConsumable(stylo));
    }
}
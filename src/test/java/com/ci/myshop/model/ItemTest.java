package com.ci.myshop.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    Item tv = new Item("télé", 120, 5);

    @Test
    void display() {
        tv.setId(2);
        assertEquals("télé 2 120.0 5", tv.display());
    }

    @Test
    void getName() {
        assertEquals("télé", tv.getName());
    }

    @Test
    void setName() {
        tv.setName("TV-LCD");
        assertEquals("TV-LCD", tv.getName());
    }

    @Test
    void getId() {
        assertEquals(1, tv.getId());
    }

    @Test
    void setId() {
        Item radio = new Item("radio", 40, 12);
        radio.setId(2);
        int testID = radio.getId();
        assertEquals(2, testID);
    }

    @Test
    void getPrice() {
        assertEquals(120.0, tv.getPrice());
    }

    @Test
    void setPrice() {
        tv.setPrice(170);
        assertEquals(170.0, tv.getPrice());
    }

    @Test
    void getNbrElt() {
        assertEquals(5, tv.getNbrElt());
    }

    @Test
    void setNbrElt() {
        tv.setNbrElt(8);
        assertEquals(8, tv.getNbrElt());
    }
}